Summary: WordPress-MU multi-user blogging software
URL: http://mu.wordpress.org/latest.tar.gz
Name: wordpress-mu
Version: 2.6.5
Release: 1%{?dist}
Group: Applications/Publishing
License: GPLv2
Source0: %{name}-%{version}.tar.gz
Source1: wordpress-mu-httpd-conf
Source2: README.fedora.wordpress-mu
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: php >= 4.1.0, httpd, php-mysql
BuildArch: noarch

%description
WordPress-MU is a derivative of the WordPress blogging codebase, to allow
one instance to serve multiple users.

%prep
%setup -q

# disable-wordpress-core-update, updates are always installed via rpm
#
# the sed script from wordpress.spec doesn't work due to if/else statement in upstream wordpress-mu's update.php
# sed -i -e "s,add_action,#add_action,g" wp-includes/update.php

echo "<?php // update script removed, updates via RPMs for Fedora ?>" > wp-includes/update.php

%build


%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{_datadir}/wordpress-mu
mkdir -p %{buildroot}%{_sysconfdir}/wordpress-mu
install -m 0644 -D -p %{SOURCE1} %{buildroot}%{_sysconfdir}/httpd/conf.d/wordpress-mu.conf
install -m 0644 -D -p %{SOURCE2} %{buildroot}%{_docdir}/%{name}-%{version}/README.fedora

cp -pr * %{buildroot}%{_datadir}/wordpress-mu

# fix weird upstream perms...
chmod 0644 %{buildroot}%{_datadir}/wordpress-mu/wp-includes/js/tinymce/plugins/spellchecker/css/content.css
chmod 0664 %{buildroot}%{_datadir}/wordpress-mu/wp-includes/js/tinymce/plugins/spellchecker/editor_plugin.js
chmod 0664 %{buildroot}%{_datadir}/wordpress-mu/wp-includes/js/tinymce/plugins/spellchecker/config.php


# since we're using /etc/httpd/conf.d, don't do the funky .htaccess stuff
# that the installer wants
echo "# please see /etc/httpd/conf.d/wordpress-mu.conf" > %{buildroot}%{_datadir}/wordpress-mu/htaccess.dist


# Remove empty files to make rpmlint happy
find %{buildroot} -empty -exec rm -f {} \;
# These are docs, remove them from here, docify them later
rm -f %{buildroot}%{_datadir}/wordpress-mu/{license.txt,README.txt}
# remove these as well, detritus
rm -f %{buildroot}%{_datadir}/wordpress-mu/{wordpress-mu-httpd-conf,README.fedora}


%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%config(noreplace) %{_sysconfdir}/httpd/conf.d/wordpress-mu.conf
%dir %{_datadir}/wordpress-mu
%{_datadir}/wordpress-mu/htaccess.dist
%{_datadir}/wordpress-mu/wp-admin
%{_datadir}/wordpress-mu/wp-content
%{_datadir}/wordpress-mu/wp-includes
%{_datadir}/wordpress-mu/index.php
%doc license.txt
%doc README.txt
%{_datadir}/wordpress-mu/index-install.php
%{_datadir}/wordpress-mu/wp-activate.php
%{_datadir}/wordpress-mu/wp-atom.php
%{_datadir}/wordpress-mu/wp-app.php
%{_datadir}/wordpress-mu/wp-blog-header.php
%{_datadir}/wordpress-mu/wp-comments-post.php
%{_datadir}/wordpress-mu/wp-commentsrss2.php
%{_datadir}/wordpress-mu/wp-config-sample.php
%{_datadir}/wordpress-mu/wp-cron.php
%{_datadir}/wordpress-mu/wp-feed.php
%{_datadir}/wordpress-mu/wp-load.php
%{_datadir}/wordpress-mu/wp-links-opml.php
%{_datadir}/wordpress-mu/wp-login.php
%{_datadir}/wordpress-mu/wp-mail.php
%{_datadir}/wordpress-mu/wp-pass.php
%{_datadir}/wordpress-mu/wp-rdf.php
%{_datadir}/wordpress-mu/wp-rss.php
%{_datadir}/wordpress-mu/wp-rss2.php
%{_datadir}/wordpress-mu/wp-settings.php
%{_datadir}/wordpress-mu/wpmu-settings.php
%{_datadir}/wordpress-mu/wp-signup.php
%{_datadir}/wordpress-mu/wp-trackback.php
%{_datadir}/wordpress-mu/xmlrpc.php
%{_datadir}/wordpress-mu/favicon.ico
%dir %{_sysconfdir}/wordpress-mu

%changelog
* Mon Dec  1 2008 Bret McMillan <bretm@redhat.com> - 2.6.5-1
- Update to 2.6.5
- http://wordpress.org/development/2008/11/wordpress-265/
- http://ocaoimh.ie/2008/11/25/wordpress-mu-265/
- Fixes 1 XSS security issue, 3 bugs

* Wed Sep  3 2008 Bret McMillan <bretm@redhat.com> - 2.6.1-1
- update to 2.6.1

* Tue Jul 29 2008 Bret McMillan <bretm@redhat.com> - 2.6-7
- added build section to the spec file

* Wed Jul 29 2008 Bret McMillan <bretm@redhat.com> - 2.6-6
- rebasing to wpmu 2.6

* Fri May 30 2008 Bret McMillan <bretm@redhat.com> - 1.5.1-2
- trying to clean up how we're dealing with htaccess.dist

* Mon May 26 2008 Bret McMillan <bretm@redhat.com> - 1.5.1-1
- update to 1.5.1; getting ready for Fedora submission

* Fri Apr 25 2008 Bret McMillan <bretm@redhat.com> - 1.3.3-1
- initial version; copying heuristics from the base fedora wordpress pkg

